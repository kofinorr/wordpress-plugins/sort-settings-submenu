# Sort Settings Submenu
Rearrange the "Settings" submenu by alphabetic order to find a plugin setting page more easily.

## Requirements
* Require WordPress 4.7+ / Tested up to 5.6
* Require PHP 5.6


## Installation

### Manual
- Download and install the plugin using the built-in WordPress plugin installer.
- No settings necessary, it's a Plug-and-Play plugin !

### Composer
- Add the following repository source : 
```json
{
    "type": "vcs",
    "url": "https://gitlab.com/kalimorr/wordpress-plugins/show-page-template.git"
}
```
- Include the following line into your composer file for last master's commits or a tag released.
```json
"kalimorr/show-page-template": "dev-master"
```

- No settings necessary, it's a Plug-and-Play plugin !

## License
"Sort Settings Submenu" is licensed under the GPLv3 or later.